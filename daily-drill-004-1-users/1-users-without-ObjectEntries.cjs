const users = {
  John: {
    age: 24,
    desgination: "Senior Golang Developer",
    interests: ["Chess, Reading Comics, Playing Video Games"],
    qualification: "Masters",
    nationality: "Greenland",
  },
  Ron: {
    age: 19,
    desgination: "Intern - Golang",
    interests: ["Video Games"],
    qualification: "Bachelor",
    nationality: "UK",
  },
  Wanda: {
    age: 24,
    desgination: "Intern - Javascript",
    interests: ["Piano"],
    qualification: "Bachaelor",
    nationality: "Germany",
  },
  Rob: {
    age: 34,
    desgination: "Senior Javascript Developer",
    interests: ["Walking his dog, Cooking"],
    qualification: "Masters",
    nationality: "USA",
  },
  Pike: {
    age: 23,
    desgination: "Python Developer",
    interests: ["Listing Songs, Watching Movies"],
    qualification: "Bachaelor's Degree",
    nationality: "Germany",
  },
};

/*

Q1 Find all users who are interested in playing video games.
Q2 Find all users staying in Germany.
Q3 Sort users based on their seniority level 
   for Designation - Senior Developer > Developer > Intern
   for Age - 20 > 10
Q4 Find all users with masters Degree.
Q5 Group users based on their Programming language mentioned in their designation.

NOTE: Do not change the name of this file

*/

const usersWhoAreGamers = Object.getOwnPropertyNames(users)
  .filter((username) => {
    return users[username].interests[0].includes("Video Games");
  })
  .map((username) => {
    return { [username]: users[username] };
  });
console.log(usersWhoAreGamers);

const usersStayingInGermany = Object.getOwnPropertyNames(users)
  .filter((username) => {
    return users[username].nationality.includes("Germany");
  })
  .map((username) => {
    return { [username]: users[username] };
  });
// console.log(usersStayingInGermany);

const sortedUsers = Object.getOwnPropertyNames(users)
  .sort((usernameA, usernameB) => {
    let designationUserA = users[usernameA].desgination.includes("Senior")
      ? 2
      : users[usernameA].desgination.includes("Developer")
      ? 1
      : 0;
    let desginationUserB = users[usernameB].desgination.includes("Senior")
      ? 2
      : users[usernameB].desgination.includes("Developer")
      ? 1
      : 0;

    if (designationUserA == desginationUserB) {
      return users[usernameB].age - users[usernameA].age;
    } else if (designationUserA > desginationUserB) {
      return -1;
    } else {
      return 1;
    }
  })
  .map((username) => {
    return { [username]: users[username] };
  });
// console.log(sortedUsers);

const userWithMasters = Object.getOwnPropertyNames(users)
  .filter((username) => {
    return users[username].qualification.includes("Masters");
  })
  .map((username) => {
    return { [username]: users[username] };
  });

// console.log(userWithMasters);

const groupByProgrammingLanguage = Object.getOwnPropertyNames(users).reduce(
  (accumulator, username) => {
    let programmingLanguageOfUser = users[username].desgination
      .replaceAll("Senior", "")
      .replaceAll("Developer", "")
      .replaceAll("Intern", "")
      .replaceAll("-", "")
      .trim();
    if (programmingLanguageOfUser in accumulator == false) {
      accumulator[programmingLanguageOfUser] = [];
    }
    let userObject = Object.fromEntries([[username, users[username]]]);
    accumulator[programmingLanguageOfUser].push(userObject);
    return accumulator;
  },
  {}
);
// console.log(groupByProgrammingLanguage);
