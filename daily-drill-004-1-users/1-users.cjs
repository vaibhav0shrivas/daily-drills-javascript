const users = {
  John: {
    age: 24,
    desgination: "Senior Golang Developer",
    interests: ["Chess, Reading Comics, Playing Video Games"],
    qualification: "Masters",
    nationality: "Greenland",
  },
  Ron: {
    age: 19,
    desgination: "Intern - Golang",
    interests: ["Video Games"],
    qualification: "Bachelor",
    nationality: "UK",
  },
  Wanda: {
    age: 24,
    desgination: "Intern - Javascript",
    interests: ["Piano"],
    qualification: "Bachaelor",
    nationality: "Germany",
  },
  Rob: {
    age: 34,
    desgination: "Senior Javascript Developer",
    interests: ["Walking his dog, Cooking"],
    qualification: "Masters",
    nationality: "USA",
  },
  Pike: {
    age: 23,
    desgination: "Python Developer",
    interests: ["Listing Songs, Watching Movies"],
    qualification: "Bachaelor's Degree",
    nationality: "Germany",
  },
};

/*

Q1 Find all users who are interested in playing video games.
Q2 Find all users staying in Germany.
Q3 Sort users based on their seniority level 
   for Designation - Senior Developer > Developer > Intern
   for Age - 20 > 10
Q4 Find all users with masters Degree.
Q5 Group users based on their Programming language mentioned in their designation.

NOTE: Do not change the name of this file

*/

const usersWhoAreGamers = Object.fromEntries(
  Object.entries(users).filter((user) => {
    if (user[1].interests[0].toLowerCase().includes("video games")) {
      return true;
    }
    return false;
  })
);
// console.log(usersWhoAreGamers);

const userfromGermany = Object.fromEntries(
  Object.entries(users).filter((user) => {
    if (user[1].nationality.includes("Germany")) {
      return true;
    }
    return false;
  })
);
// console.log(userfromGermany);

const userWithMasters = Object.fromEntries(
  Object.entries(users).filter((user) => {
    if (user[1].qualification.includes("Masters")) {
      return true;
    }
    return false;
  })
);
// console.log(userWithMasters);

const groupByProgrammingLanguage = Object.entries(users).reduce(
  (accumulator, user) => {
    let programmingLanguageOfUser = user[1].desgination
      .replaceAll("Senior", "")
      .replaceAll("Developer", "")
      .replaceAll("Intern", "")
      .replaceAll("-", "")
      .trim();
    if (programmingLanguageOfUser in accumulator == false) {
      accumulator[programmingLanguageOfUser] = [];
    }
    let userObject = Object.fromEntries([[user[0], user[1]]]);
    accumulator[programmingLanguageOfUser].push(userObject);
    return accumulator;
  },
  {}
);

const sortedUsersBySeniority = Object.entries(users).sort((userA, userB) => {
  let desginationUserA = userA[1].desgination.includes("Senior")
    ? 2
    : userA[1].desgination.includes("Developer")
    ? 1
    : 0;
  let desginationUserB = userB[1].desgination.includes("Senior")
    ? 2
    : userB[1].desgination.includes("Developer")
    ? 1
    : 0;

  if (desginationUserA == desginationUserB) {
    return userB[1].age - userA[1].age;
  } else if (desginationUserA > desginationUserB) {
    return -1;
  } else {
    return 1;
  }
});

console.log(sortedUsersBySeniority);
