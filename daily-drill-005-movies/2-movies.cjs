const favouritesMovies = {
  Matrix: {
    imdbRating: 8.3,
    actors: ["Keanu Reeves", "Carrie-Anniee"],
    oscarNominations: 2,
    genre: ["sci-fi", "adventure"],
    totalEarnings: "$680M",
  },
  FightClub: {
    imdbRating: 8.8,
    actors: ["Edward Norton", "Brad Pitt"],
    oscarNominations: 6,
    genre: ["thriller", "drama"],
    totalEarnings: "$350M",
  },
  Inception: {
    imdbRating: 8.3,
    actors: ["Tom Hardy", "Leonardo Dicaprio"],
    oscarNominations: 12,
    genre: ["sci-fi", "adventure"],
    totalEarnings: "$870M",
  },
  "The Dark Knight": {
    imdbRating: 8.9,
    actors: ["Christian Bale", "Heath Ledger"],
    oscarNominations: 12,
    genre: ["thriller"],
    totalEarnings: "$744M",
  },
  "Pulp Fiction": {
    imdbRating: 8.3,
    actors: ["Sameul L. Jackson", "Bruce Willis"],
    oscarNominations: 7,
    genre: ["drama", "crime"],
    totalEarnings: "$455M",
  },
  Titanic: {
    imdbRating: 8.3,
    actors: ["Leonardo Dicaprio", "Kate Winslet"],
    oscarNominations: 13,
    genre: ["drama"],
    totalEarnings: "$800M",
  },
};

/*
    NOTE: For all questions, the returned data must contain all the movie information including its name.

    Q1. Find all the movies with total earnings more than $500M. 
    Q2. Find all the movies who got more than 3 oscarNominations and also totalEarning are more than $500M.
    Q.3 Find all movies of the actor "Leonardo Dicaprio".
    Q.4 Sort movies (based on IMDB rating)
        if IMDB ratings are same, compare totalEarning as the secondary metric.
    Q.5 Group movies based on genre. Priority of genres in case of multiple genres present are:
        drama > sci-fi > adventure > thriller > crime

    NOTE: Do not change the name of this file
*/

const moviesWithEarningsOver500M = Object.fromEntries(
  Object.entries(favouritesMovies).filter((movieDetails) => {
    //movieDetails[0] => Movie name(string),movieDetails[1] => object
    if (
      movieDetails[1].totalEarnings.includes("B") ||
      movieDetails[1].totalEarnings.includes("b")
    ) {
      return true;
    }
    let totalCollection = parseFloat(
      movieDetails[1].totalEarnings.replace("$", "")
    );
    if (totalCollection > 500) {
      return true;
    } else {
      return false;
    }
  })
);
// console.log(moviesWithEarningsOver500M);

const moviesWith3OscarNominationandOver500m = Object.fromEntries(
  Object.entries(moviesWithEarningsOver500M).filter((movieDetails) => {
    if (movieDetails[1].oscarNominations > 3) {
      return true;
    } else {
      return false;
    }
  })
);
// console.log(moviesWith3OscarNominationandOver500m);

let actorName = "Leonardo Dicaprio";
const moviesWithActor = Object.fromEntries(
  Object.entries(favouritesMovies).filter((movieDetails) => {
    if (movieDetails[1].actors.includes(actorName)) {
      return true;
    } else {
      return false;
    }
  })
);
// console.log(moviesWithActor);

const sortedMoviesByRatings = Object.entries(favouritesMovies).sort(
  (movieL, movieR) => {
    if (movieL[1].imdbRating < movieR[1].imdbRating) {
      return 1;
    } else if (movieL[1].imdbRating > movieR[1].imdbRating) {
      return -1;
    } else {
      return movieR[1].totalEarnings.localeCompare(movieL[1].totalEarnings);
    }
  }
);
// console.log(sortedMoviesByRatings);

const moviesGroupedByGenre = Object.entries(favouritesMovies).reduce(
  (accumulator, movieDetails) => {
    if (movieDetails[1].genre.includes("drama")) {
      accumulator["drama"].push(movieDetails);
    } else if (movieDetails[1].genre.includes("sci-fi")) {
      accumulator["sci-fi"].push(movieDetails);
    } else if (movieDetails[1].genre.includes("adventure")) {
      accumulator["adventure"].push(movieDetails);
    } else if (movieDetails[1].genre.includes("thriller")) {
      accumulator["thriller"].push(movieDetails);
    } else if (movieDetails[1].genre.includes("crime")) {
      accumulator["crime"].push(movieDetails);
    } else {
      accumulator["others"].push(movieDetails);
    }
    return accumulator;
  },
  //drama > sci-fi > adventure > thriller > crime
  {
    drama: [],
    "sci-fi": [],
    adventure: [],
    thriller: [],
    crime: [],
    others: [],
  }
);
// console.log(moviesGroupedByGenre);
