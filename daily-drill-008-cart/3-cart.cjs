const products = [{
    shampoo: {
        price: "$50",
        quantity: 4
    },
    "Hair-oil": {
        price: "$40",
        quantity: 2,
        sealed: true
    },
    comb: {
        price: "$12",
        quantity: 1
    },
    utensils: [
        {
            spoons: { quantity: 2, price: "$8" }
        }, {
            glasses: { quantity: 1, price: "$70", type: "fragile" }
        }, {
            cooker: { quantity: 4, price: "$900" }
        }
    ],
    watch: {
        price: "$800",
        quantity: 1,
        type: "fragile"
    }
}];


/*

Q1. Find all the items with price more than $65.
Q2. Find all the items where quantity ordered is more than 1.
Q.3 Get all items which are mentioned as fragile.
Q.4 Find the least and the most expensive item for a single quantity.
Q.5 Group items based on their state of matter at room temperature (Solid, Liquid Gas)

NOTE: Do not change the name of this file

*/
const productsWithPriceOver65 = Object.fromEntries(Object.entries(products[0])
    .filter((cartItem) => {
        let priceOfCurrentItem = -1;
        let isThereSUbItemOver65 = false;
        if ("price" in cartItem[1]) {
            priceOfCurrentItem = Number(cartItem[1].price.replace("$", "").replaceAll(",", ""));

        } else if (Array.isArray(cartItem[1])) {
            cartItem[1] = cartItem[1].filter((subItemObject) => {
                let subItemDetail = Object.values(subItemObject)[0];
                if ("price" in subItemDetail) {
                    priceOfCurrentItem = Number(subItemDetail.price.replace("$", "").replaceAll(",", ""));
                }
                isThereSUbItemOver65 += (priceOfCurrentItem > 65);
                return priceOfCurrentItem > 65;
            });
        }
        return isThereSUbItemOver65 + (priceOfCurrentItem > 65);
    }));

// console.log(productsWithPriceOver65);


const productsWithQuantityOver1 = Object.fromEntries(Object.entries(products[0])
    .filter((cartItem) => {
        let QuantityOfCurrentItem = -1;
        let isThereSUbItemWithQuantityOver1 = false;
        if ("quantity" in cartItem[1]) {
            QuantityOfCurrentItem = cartItem[1].quantity;
        } else if (Array.isArray(cartItem[1])) {
            cartItem[1] = cartItem[1].filter((subItemObject) => {
                let subItemDetail = Object.values(subItemObject)[0];
                if ("quantity" in subItemDetail) {
                    QuantityOfCurrentItem = subItemDetail.quantity;
                }
                isThereSUbItemWithQuantityOver1 += (QuantityOfCurrentItem > 1);
                return QuantityOfCurrentItem > 1;
            });
        }
        return isThereSUbItemWithQuantityOver1 + (QuantityOfCurrentItem > 1);
    }));
// console.log(productsWithQuantityOver1);


const productFragile = Object.fromEntries(Object.entries(products[0])
    .filter((cartItem) => {
        let isFragile = false;
        let isThereSUbItemFragile = false;
        if ("type" in cartItem[1]) {
            isFragile = (cartItem[1].type === "fragile");
        } else if (Array.isArray(cartItem[1])) {
            cartItem[1] = cartItem[1].filter((subItemObject) => {
                let subItemDetail = Object.values(subItemObject)[0];
                isFragile = false;
                if ("type" in subItemDetail) {
                    isFragile = (subItemDetail.type === "fragile");
                }
                isThereSUbItemFragile += isFragile;
                return isFragile;
            });
        }
        return isThereSUbItemFragile + isFragile;
    }));
// console.log(productFragile);


//The following are done after deadline


const leastExpensiveAndMostExpensive = Object.entries(products[0])
    .reduce((accumulator, currentCartItem) => {
        let priceForOneUnitCurrentItem;
        if ("price" in currentCartItem[1] && "quantity" in currentCartItem[1]) {
            priceForOneUnitCurrentItem = Number(currentCartItem[1].price.replace("$", "").replaceAll(",", "")) / currentCartItem[1].quantity;

            if (priceForOneUnitCurrentItem > accumulator.CostliestItemPrice) {
                accumulator.CostliestItem = Object.fromEntries([currentCartItem]);
                accumulator.CostliestItemPrice = priceForOneUnitCurrentItem;
            }
            if (priceForOneUnitCurrentItem < accumulator.CheapestItemPrice) {
                accumulator.CheapestItem = Object.fromEntries([currentCartItem]);
                accumulator.CheapestItemPrice = priceForOneUnitCurrentItem;
            }

        } else if (Array.isArray(currentCartItem[1])) {
            currentCartItem[1].forEach((subItemObject) => {
                let subItemkey = Object.keys(subItemObject)[0];
                let subItemDetail = subItemObject[subItemkey]
                if ("price" in subItemDetail && "quantity" in subItemDetail) {
                    priceForOneUnitCurrentItem = Number(subItemDetail.price.replace("$", "").replaceAll(",", "")) / subItemDetail.quantity;

                    if (priceForOneUnitCurrentItem > accumulator.CostliestItemPrice) {
                        accumulator.CostliestItem = subItemObject
                        accumulator.CostliestItemPrice = priceForOneUnitCurrentItem;
                    }
                    if (priceForOneUnitCurrentItem < accumulator.CheapestItemPrice) {
                        accumulator.CheapestItem = subItemObject;
                        accumulator.CheapestItemPrice = priceForOneUnitCurrentItem;
                    }

                }

            });


        }
        return accumulator;
    },
        {
            CostliestItem: {},
            CostliestItemPrice: Number.NEGATIVE_INFINITY,
            CheapestItem: {},
            CheapestItemPrice: Number.POSITIVE_INFINITY
        }
    );

// console.log(leastExpensiveAndMostExpensive);








