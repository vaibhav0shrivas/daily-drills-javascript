/*
* Use asynchronous callbacks ONLY wherever possible.
* Error first callbacks must be used always.
* Each question's output has to be stored in a json file.
* Each output file has to be separate.

* Ensure that error handling is well tested.
* After one question is solved, only then must the next one be executed. 
* If there is an error at any point, the subsequent solutions must not get executed.

* Store the given data into data.json
* Read the data from data.json
* Perfom the following operations.

1. Retrieve data for ids : [2, 13, 23].
2. Group data based on companies.
{ "Scooby Doo": [], "Powerpuff Brigade": [], "X-Men": []}
3. Get all data for company Powerpuff Brigade
4. Remove entry with id 2.
5. Sort data based on company name. If the company name is same, use id as the secondary sort metric.
6. Swap position of companies with id 93 and id 92.
7. For every employee whose id is even, add the birthday to their information. The birthday can be the current date found using `Date`.

NOTE: Do not change the name of this file

*/

const fs = require("fs");
const path = require("path");

const addBirthdays = function (Records) {
    let result = { ...Records };

    result["employees"].forEach(employee => {
        if (employee.id % 2 == 0) {
            employee.birthday = new Date().toISOString().slice(0, 10);
        }
    });

    return result;
};


const swapTheseIdsComapny = function (Records, searchID1, searchID2) {
    let result = { ...Records };

    const index1 = result["employees"].indexOf(result["employees"].find((employee) => {
        return employee.id === searchID1;
    }));
    const index2 = result["employees"].indexOf(result["employees"].find((employee) => {
        return employee.id === searchID2;
    }));

    if (index1 >= 0 && index2 >= 0) {
        let temp = result["employees"][index1]["company"];
        result["employees"][index1]["company"] = result["employees"][index2]["company"];
        result["employees"][index2]["company"] = temp;
    } else {
        console.error(`ID not found!`);
    }

    return result;
};


const sortByCompanyName = function (Records) {
    let toBeSorted = Records["employees"];

    toBeSorted.sort((employeeA, employeeB) => {
        if (employeeA["company"].localeCompare(employeeB["company"]) == 0) {
            return employeeA.id - employeeB.id;
        } else {
            return employeeA["company"] > employeeB["company"] ? 1 : -1;
        }

    });
    Records["employees"] = toBeSorted

    return Records;

};

const removeEmployeeDataForID = function (deletionID, Records) {
    let employeeDataAfterDeletion = Records["employees"].filter((employeeDetail) => {
        if (employeeDetail.id === deletionID) {
            return false;
        } else {
            return true;
        }
    });

    let result = {};
    result["employees"] = (employeeDataAfterDeletion);

    return result;

};

const getEmployeeDataForID = function (searchID, Records) {
    let employeeDataForID = Records["employees"].find((employeeDetail) => {
        if (employeeDetail.id === searchID) {
            return true;
        }
    });

    return employeeDataForID;

};

const getEmployeeDataForCompany = function (searchComapny, Records) {
    let employeeDataForCompany = Records["employees"].filter((employeeDetail) => {
        if (employeeDetail.company === searchComapny) {
            return true;
        }
    });

    let result = { employees: [] };
    result.employees.push(employeeDataForCompany);

    return result;

};

const getDataGroupedByCompanies = function (Records) {
    let recordsGroupedByCompanies = Records["employees"]
        .reduce((accumulator, currentEmployee) => {
            if (currentEmployee["company"] in accumulator == false) {
                accumulator[currentEmployee["company"]] = [];
            }
            accumulator[currentEmployee["company"]].push(currentEmployee);
            return accumulator;

        },
            {}
        );

    return recordsGroupedByCompanies;
};

const writeToFile = function (dataToBeWritten, fileName, callback) {
    fs.writeFile(path.join(__dirname, `/${fileName}.json`), JSON.stringify(dataToBeWritten), (error) => {
        if (error) {

            console.error(`Failed to write to ${fileName}.json.`);
            callback(error);
        } else {

            console.error(`Wrote to ${fileName}.json.`);
            callback(null);
        }
    }
    );
};


const main = function (callbackForMain) {
    const pathForDataFile = path.join(__dirname, "/data.json");

    fs.readFile(pathForDataFile, (error, data) => {
        if (error) {

            console.error(`Failed to read from data.json.`);
            callbackForMain(error)
        } else {

            const dataFromJSONfile = JSON.parse(data);
            if ("employees" in dataFromJSONfile == false) {
                callbackForMain(new Error(`Data is invalid.`));
                return;
            }

            let resultforIds = { employees: [] };
            let listOfSearchIDs = [2, 13, 23];

            listOfSearchIDs.forEach((searchID) => {
                resultforIds.employees.push(getEmployeeDataForID(searchID, dataFromJSONfile));

            });
            writeToFile(resultforIds, "Output1-search-for-id-[2,13,23]", (error) => {
                if (error) {

                    console.error(`Failed call to writeToFile while writing to Output1.`);
                    callbackForMain(error)
                } else {

                    let resultforGroupedDataByCompanies = getDataGroupedByCompanies(dataFromJSONfile);

                    writeToFile(resultforGroupedDataByCompanies, "Output2-group-by-companies", (error) => {
                        if (error) {
                            console.error(`Failed call to writeToFile while writing to Output2.`);
                            callbackForMain(error);

                        } else {
                            let companyNameToBeSearched = "Powerpuff Brigade";
                            let resultForGivenCompany = getEmployeeDataForCompany(companyNameToBeSearched, dataFromJSONfile);

                            writeToFile(resultForGivenCompany, "Output3-search-for-comapany-Powerpuff-Brigade", (error) => {
                                if (error) {
                                    console.error(`Failed call to writeToFile while writing to Output3.`);
                                    callbackForMain(error);

                                } else {
                                    let resultWithoutGivenId = removeEmployeeDataForID(2, dataFromJSONfile);

                                    writeToFile(resultWithoutGivenId, "Output4-data-without-ID-2", (error) => {
                                        if (error) {
                                            console.error(`Failed to write to data.json`);
                                            callbackForMain(error);
                                            
                                        } else {
                                            let resultForSortedData = sortByCompanyName(dataFromJSONfile);

                                            writeToFile(resultForSortedData, "Output5-sorted-data-by-company", (error) => {
                                                if (error) {
                                                    console.error("Failed call to writeToFile for writing Output5.");
                                                    callbackForMain(error);

                                                } else {
                                                    let resultOfSwapping = swapTheseIdsComapny(dataFromJSONfile, 93, 92);

                                                    writeToFile(resultOfSwapping, "Output6-swapped-company", (error) => {
                                                        if (error) {
                                                            console.error("Failed call to writeToFile for writing Output6.");
                                                            callbackForMain(error);

                                                        } else {
                                                            let resultOfAddingBirthdays = addBirthdays(dataFromJSONfile);

                                                            writeToFile(resultOfAddingBirthdays, "Output7-with-Birthday", (error) =>{
                                                                if(error){
                                                                    console.error("Failed call to writeToFile for writing Output7.");
                                                                    callbackForMain(error);

                                                                }else{
                                                                    callbackForMain(null);
                                                                }
                                                            });

                                                        }

                                                    });

                                                }
                                            });

                                        }
                                    });

                                }

                            });


                        }
                    });
                }
            });

        }
    });





};

main((error) => {
    if (error) {
        console.error("Failed to execute main().");
    } else {
        console.log("Sucessfully executed main().");
    }
});