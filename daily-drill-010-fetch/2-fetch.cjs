
/*
NOTE: Do not change the name of this file

* Ensure that error handling is well tested.
* If there is an error at any point, the subsequent solutions must not get executed.
* Solutions without error handling will get rejected and you will be marked as having not completed this drill.

* Usage of async and await is not allowed.

Users API url: https://jsonplaceholder.typicode.com/users
Todos API url: https://jsonplaceholder.typicode.com/todos

Users API url for specific user ids : https://jsonplaceholder.typicode.com/users?id=2302913
Todos API url for specific user Ids : https://jsonplaceholder.typicode.com/todos?userId=2321392

Using promises and the `fetch` library, do the following. 

1. Fetch all the users
2. Fetch all the todos
3. Use the promise chain and fetch the users first and then the todos.
4. Use the promise chain and fetch the users first and then all the details for each user.
5. Use the promise chain and fetch the first todo. Then find all the details for the user that is associated with that todo

NOTE: If you need to install `node-fetch` or a similar libary, let your mentor know before doing so along with the reason why. No other exteral libraries are allowed to be used.

Usage of the path libary is recommended


*/

const UsersUrl = "https://jsonplaceholder.typicode.com/users";
const TodosUrl = "https://jsonplaceholder.typicode.com/todos";
// Users API url for specific user ids : https://jsonplaceholder.typicode.com/users?id=2302913
// Todos API url for specific user Ids : https://jsonplaceholder.typicode.com/todos?userId=2321392

function fetchData(url) {
    return new Promise((resolve, reject) => {
        fetch(url)
            .then((response) => {
                if (response.ok === undefined) {
                    throw new Error("No Response");
                } else {
                    resolve(response.json());
                }
            })
            .catch((error) => {
                reject(error);
            });

    });

};



function main() {
    let userData, todoData;


    fetchData(UsersUrl)
        .then((data) => {

            userData = data;
            console.log("Q1-Q2-Q3");
            console.log(userData);

            return fetchData(TodosUrl);

        })
        .then((data) => {

            todoData = data;
            console.log("Q1-Q2-Q3");
            console.log(todoData);

            return fetchData(UsersUrl);


        })
        .then((data) => {

            userData = data;

            return resultantPriomise = Promise.all(userData.map(userDetail => {
                if ("id" in userDetail) {
                    const userURLforID = `https://jsonplaceholder.typicode.com/users?id=${userDetail.id}`;
                    return fetchData(userURLforID);
                }

            }));


        })
        .then((dataForEachUserID) => {

            console.log("Q4");
            dataForEachUserID.forEach((data) => {
                console.log(data);
            });

            return fetchData(TodosUrl);

        })
        .then((data) => {
            if (data.length == 0) {
                throw new Error("No todo data recieved");
            } else {
                let firstTodo = data[0];
                console.log("First todo ",firstTodo);
                if("userId" in firstTodo){
                    const urlForFirstTodoUser = `https://jsonplaceholder.typicode.com/users?id=${firstTodo.userId}`;
                    return fetchData(urlForFirstTodoUser);

                }else{
                    throw new Error("Wrong Todo Data");

                }                
            }
        })
        .then((data) => {
            console.log("Q5");
            console.log(data);

        })
        .catch((error) => {
            console.error("Error Occured",error);
        });



};

main();