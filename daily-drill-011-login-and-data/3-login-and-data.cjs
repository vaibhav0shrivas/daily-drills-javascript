/*
NOTE: Do not change the name of this file

NOTE: For all file operations use promises with fs. Promisify the fs callbacks rather than use fs/promises.

Q1. Create 2 files simultaneously (without chaining).
Wait for 2 seconds and starts deleting them one after another. (in order)
(Finish deleting all the files no matter what happens with the creation code. Ensure that this is tested well)


Q2. Create a new file with lipsum data (you can google and get this). 
Do File Read and write data to another file
Delete the original file 
Using promise chaining
*/
const loremipsumsataString = ` Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis ac magna sapien. Duis lacus neque, tempor vulputate lobortis at, aliquet sed nisl. Nulla sapien neque, feugiat nec pellentesque ut, interdum non nisl. In et nulla eu risus lobortis dignissim. Sed metus quam, maximus sed ante eu, consectetur scelerisque risus. Nulla at orci eget eros dapibus eleifend eget quis mauris. In malesuada mi in mauris hendrerit fermentum vitae et urna. Praesent libero lacus, dignissim vitae justo non, pretium tempor ipsum. Maecenas massa ex, venenatis eget aliquet quis, gravida interdum odio. Cras ut ullamcorper lacus. Etiam mattis faucibus mi in dapibus. Cras sit amet turpis in lorem pharetra porta.

Pellentesque eget augue sollicitudin, pretium justo et, accumsan lectus. Aenean quam eros, gravida in maximus ut, condimentum dictum velit. Vestibulum tempus nulla sit amet felis accumsan faucibus. Nam non sapien nec augue sodales imperdiet ac vitae nisl. Aenean vitae erat massa. Maecenas semper lorem eget mauris semper euismod. Vestibulum tempus placerat quam, vel rhoncus tellus aliquam a. Cras semper sem vitae quam fringilla, vel venenatis est dignissim. Morbi eu turpis enim. Vestibulum congue tellus vel dolor porta facilisis. Mauris suscipit ultrices sapien id fringilla. Vivamus vitae tempor ligula, ac faucibus risus. Vestibulum posuere enim sed sodales molestie. Curabitur finibus neque ac lectus viverra tempus. Vivamus volutpat at leo sit amet porttitor. `;

const path = require("path");
const fs = require("fs");

function login(user, val) {
    if (val % 2 === 0) {
        return Promise.resolve(user);
    } else {
        return Promise.reject(new Error("User not found"));
    }
}

function getData() {
    return Promise.resolve([
        {
            id: 1,
            name: "Test",
        },
        {
            id: 2,
            name: "Test 2",
        }
    ]);
}

function logData(user, activity) {
    // use promises and fs to save activity in some file
    let data = JSON.stringify({
        "time stamp": new Date().toString(),
        [user.name]: activity
    },);
    return new Promise((resolve, reject) => {
        fs.writeFile(path.join(__dirname, "LogFile.txt"), "\n" + data, { flag: "a" }, (error) => {
            if (error) {
                reject(error);
            } else {

                resolve();
            }
        });
    });
}

/*
Q3.
Use appropriate methods to 
A. login with value 3 and call getData once login is successful
B. Write logic to logData after each activity for each user. Following are the list of activities
    "Login Success"
    "Login Failure"
    "GetData Success"
    "GetData Failure"

    Call log data function after each activity to log that activity in the file.
    All logged activity must also include the timestamp for that activity.
    
    You may use any data you want as the `user` object as long as it represents a valid user (eg: For you, for your friend, etc)
    All calls must be chained unless mentioned otherwise.
    
*/

function intiateProcessForUser(passdUser) {
    if ("id" in passdUser == false) {
        return;
    }

    login(passdUser, passdUser.id)
        .then((user) => {
            logData(user, "Login Success");
            return getData();
        })
        .then((data) => {

            console.log("Rcieved data", data);
            logData(passdUser, "GetData Success");

        })
        .catch((error) => {
            if (error.message === "User not found") {
                logData(passdUser, "Login Failure");
            } else {
                logData(passdUser, "GetData Failure");
            }

        });
}

function problem3() {
    const users = [
        {
            id: 1,
            name: "Batman",
        },
        {
            id: 2,
            name: "Shaktiman",
        },
        {
            id: 3,
            name: "CALL_With_Userid3",
        },
        {
            id: 4,
            name: "Luffy",
        }

    ];

    users.forEach((user) => {
        intiateProcessForUser(user)
    });


}

function createFile(fileName, data) {
    if (typeof data !== "string") {
        data = JSON.stringify(data);
    }

    return new Promise((resolve, reject) => {
        fs.writeFile(path.join(__dirname, fileName), data, (error) => {
            if (error) {

                reject(error);
            } else {

                resolve(path.join(__dirname, fileName));
            }
        });
    });

};

function deleteThisFile(filePath) {
    return new Promise((resolve, reject) => {

        fs.unlink(filePath, (error) => {
            if (error) {

                console.error(`Failed to delete ${filePath}.`);
                if (error.code === 'ENOENT') {
                    resolve()
                } else {
                    reject(new Error(JSON.stringify({ [`failed to delete`]: filePath })));

                }

            } else {

                resolve();
            }

        });
    });
};

function readFromFile(fileToBeRead, options) {
    options = Object.assign(
        {
            encoding: "utf-8",
        },
        options
    );

    return new Promise((resolve, rejects) => {

        fs.readFile(path.join(__dirname, `/${fileToBeRead}`), options.encoding, (error, dataFromFile) => {
            if (error) {

                console.error(`Failed to read ${fileToBeRead}.`);
                rejects(error);
            } else {

                console.log(`Read ${fileToBeRead} sucessfully.`);
                resolve(dataFromFile);
            }
        });
    });
};

function problem1() {
    const promiseFIle1 = createFile("File1.txt", "This is file 1.");
    const promiseFIle2 = createFile("File2.txt", "This is file 2.");

    setTimeout(() => {
        [promiseFIle1, promiseFIle2].forEach((filePathPromise) => {
            filePathPromise
                .then((filePath) => {
                    deleteThisFile(filePath);

                }).catch((error) => {
                    console.error(error);
                });

        });


    }, 2 * 1000);

};


function problem2() {

    createFile("lipsum.txt", loremipsumsataString)
        .then(() => {
            return readFromFile("lipsum.txt")

        })
        .then((data) => {
            return createFile("lipsum_copy.txt", data);

        })
        .then(() => {
            deleteThisFile(path.join(__dirname, "/lipsum.txt"));

        })
        .catch((error) => {
            console.error(error);

        });

}


function main() {
    problem1();
    problem2();
    problem3();

};

main();